const users = require('../data/users.js');
const comments = require('../data/comments.js');
const posts = require('../data/posts.js');
const reports = require('../data/reports');
const connection = require('../config/mongoConnection');

async function main() {
    const db = await connection();
    await db.dropDatabase();
    //add users
    let u1 = await users.createUser("tanaya@gmail.com", "$2y$10$vmNlR1yCOo6vDCMUhoL/GOSRpUSpOOudk5WCCOnQLzKgnDlVCOAMm", "Tanaya");
    let u1_userid = u1._id.toHexString();
    let u1_admin = await users.setAdminAccess(u1_userid);//set user1 as Admin
    let u2 = await users.createUser("soham@gmail.com", "$2y$10$NnvFxM6Zz4suodZFF27LUuQL2aJdNfejpc3gpyd/H44bDBKsPRrmC", "Soham");
    let u2_userid = u2._id.toHexString();
    let u2_admin = await users.setAdminAccess(u2_userid);//set user2 as Admin
    let u3 = await users.createUser("jia@gmail.com", "$2a$10$2hDMVORERcJpQYDxhckLpeLtdOveKhEyFKeHFAfA4Qq6D62e4KfSe", "Jia");
    let u3_userid = u3._id.toHexString();
    let u3_admin = await users.setAdminAccess(u3_userid);//set user3 as Admin
    let u4 = await users.createUser("karan@gmail.com", "$2a$10$ad7Epy4AAPfrmRruDCwocu8Kq1lpRYTIj/fiQJ5qI/qGkvv0Ng3r6", "Karan");
    let u4_userid = u4._id.toHexString();
    let u4_admin = await users.setAdminAccess(u4_userid);//set user4 as Admin
    let u5 = await users.createUser("vihaan@gmail.com", "$2a$10$32lqGi/sY3G6IEnbDOwC5e1.6OLlu92Kkc/sYNR38240vfeJBwEkm", "Vihaan");
    let u5_userid = u5._id.toHexString();
    let u6 = await users.createUser("ruhi@gmail.com", "$2a$10$ZoH1i.W07uN/8OjP1vZGf.AAMFtp/eExMdzfzlrcb7VP9GX0MvfCm", "Ruhi");
    let u6_userid = u6._id.toHexString();
    let u7 = await users.createUser("tasha@gmail.com", "$2a$10$vdXj7OW0b/rFJI3u1m/9QO1wVdVD5gWKaeBIk8t7hfVlTkokZlERy", "Tasha");
    let u7_userid = u7._id.toHexString();
    let u8 = await users.createUser("naina@gmail.com", "$2a$10$9I1P3X89WLNzHRaTNRDJEuIPJl90TavRC8Gecy0p6bCf1WX7Nih66", "Naina");
    let u8_userid = u8._id.toHexString();

    //u1 create posts
    let p1 = await posts.createPost(
        "Fudge Brownie",
        u1_userid,
        "I’m not feeling well. My mom is in town and our plan is to eat dinner and watch movies. As the sun goes down, I decide that I need a brownie. We take a break from our usual healthy lifestyle for one day a week. During one such break, we were craving something chocolatey, fudgy, ooey gooey, and just flat out slap-your-grandma delicious. Or the other time when we returned from vacation a few weeks ago, I didn’t have enough ingredients to prepare a halfway decent dinner for us, but somehow I had everything I needed to make Easy Homemade Brownies. Brownies make everything betttterr.Fudgy brownies have a higher fat-to-flour ratio than cakey ones. So add more fat -- in this case, butter and chocolate. A cakey batch has more flour and relies on baking powder for leavening. The amount of sugar and eggs does not change whether you're going fudgy or cakey.",
        ["http://localhost:3000/public/images/fudge_brownie.jpg", "http://localhost:3000/public/images/the-very-best-brownies.jpg", "http://localhost:3000/public/images/brownies3.jpeg"],
        ["Brownies"]
    );
    let p1_postid = p1._id.toHexString();
    let p2 = await posts.createPost(
        "Christmas Cookies",
        u1_userid,
        "When it comes to the holidays, there are no limits on sweets. After all, it is the only time of year when everyone puts their best foot forward in making my absolute favorite dessert…cookies that is. Christmas cookies or Christmas biscuits are traditionally sugar cookies or biscuits (though other flavours may be used based on family traditions and individual preferences) cut into various shapes related to Christmas. Modern Christmas cookies can trace their history to recipes from Medieval Europe biscuits, when many modern ingredients such as cinnamon, ginger, black pepper, almonds and dried fruit were introduced into the west. By the 16th century Christmas biscuits had become popular across Europe, with Lebkuchen being favoured in Germany and pepparkakor in Sweden, while in Norway krumkake were popular. For recipe checkout https://blogappetitblog.com/2018/12/christmas-cookie.html.",
        ["http://localhost:3000/public/images/christmascookies.jpeg", "http://localhost:3000/public/images/santacookies.jpg", "http://localhost:3000/public/images/whitecookies.jpeg"],
        ["Cookies", "Cookies-Recipe"]
    );
    let p2_postid = p2._id.toHexString();
    let p3 = await posts.createPost(
        "Cheesecake Brownies",
        u1_userid,
        "You can never go wrong with a good chocolate dessert, so here is the delicious recipe! These cheesecake brownies are a rich and fudgy chocolate brownie topped with a layer of sweetened cream cheese. A show stopping dessert that’s easy to make and always gets rave reviews! Brownies and cheesecake are both great desserts on their own, but when you combine the two, you get these fabulous cheesecake brownies that are worthy of any special occasion! So if you want to checkout how to make these checkout the recipe at https://www.dinneratthezoo.com/cheesecake-brownies/",
        ["http://localhost:3000/public/images/cheesecake-brownies-4.jpg", "http://localhost:3000/public/images/cheesebrownies2.jpeg", "http://localhost:3000/public/images/cheesebrownies3.jpeg", "http://localhost:3000/public/images/cheesebrownies4.jpeg"],
        ["Brownies", "Brownies-Recipe"]
    );
    let p3_postid = p3._id.toHexString();


    let p4 = await posts.createPost(
        "How to make flavoured homemade yogurt?",
        u2_userid,
        "There are plenty of ways to make your own flavored yogurt, but this one makes tasty frozen yogurt using fresh ingredients that you have on hand at home.You can make it with a yogurt machine, or without.Here's another perk to making your own flavored yogurt: You can make it healthier than pre-made varieties you buy at the store. Perhaps you don't want to use traditional sugar; you can substitute a healthier sweetener. And with tons of flavor combinations, you may be able to come up with some pretty appetizing concoctions. The ingredients include milk, sweetener, non-fat instant dry milk and any fruit of your choice. For details checkout - https://www.thespruceeats.com/homemade-flavored-yogurt-recipe-1807113",
        ["http://localhost:3000/public/images/yogurt1.jpeg", "http://localhost:3000/public/images/yogurt2.jpeg", "http://localhost:3000/public/images/yogurt3.jpeg"],
        ["Yoghurt", "Yogurt-Recipe"]
    );
    let p5 = await posts.createPost(
        "Best Chocolate Cookies",
        u2_userid,
        "A chocolate chip cookie is a drop cookie that features chocolate chips or chocolate morsels as its distinguishing ingredient. Chocolate chip cookies originated in the United States around 1938, when Ruth Graves Wakefield chopped up a Nestlé semi-sweet chocolate bar and added the chopped chocolate to a cookie recipe. A chocolate chocolate chip cookie uses a dough flavored with chocolate or cocoa powder, before chocolate chips are mixed in. These variations of the recipe are also referred to as ‘double’ or ‘triple’ chocolate chip cookies, depending on the combination of dough and chocolate types. Cafe Delight has the best chocolate chip cookies ever!!",
        ["http://localhost:3000/public/images/chococookies.jpeg", "http://localhost:3000/public/images/chcocookies2.jpeg", "http://localhost:3000/public/images/chcocookies3.jpeg"],
        ["Cookies"]
    );
    let p6 = await posts.createPost(
        "Amazing Milk Cookies",
        u2_userid,
        "If you love biscuits, then this milk cookie recipe is perfect for you to try. No need to worry if you have not baked cookies before, as this easy milk cookie recipe with step by step instructions will simplify things for you. If you have special guests coming in later or want to treat your better half, this milk cookie with condensed milk recipe is just great. Milk Cookies is a lip-smacking cookie recipe that is loaded with the intense taste of condensed milk, butter and caster sugar. Serve this mouth-watering recipe to your loved ones and delight their taste buds. These milk cookies+chai=divine. So here is the recipe- https://recipes.timesofindia.com/recipes/milk-cookies/rs60085694.cms",
        ["http://localhost:3000/public/images/milkcookies1.jpeg", "http://localhost:3000/public/images/milkcookies2.jpeg", "http://localhost:3000/public/images/milkcookies3.jpeg"],
        ["Cookies", "Cookies-Recipe"]
    );


    let p7 = await posts.createPost(
        "The Taste of Summer",
        u3_userid,
        "Ice cream is a sweetened frozen food typically eaten as a snack or dessert. It may be made from dairy milk or cream and is flavoured with a sweetener, either sugar or an alternative, and any spice, such as cocoa or vanilla. It can also be made by whisking a flavored cream base and liquid nitrogen together.  Colorings are usually added, in addition to stabilizers. The mixture is stirred to incorporate air spaces and cooled below the freezing point of water to prevent detectable ice crystals from forming. The result is a smooth, semi-solid foam that is solid at very low temperatures (below 2 °C or 35 °F). It becomes more malleable as its temperature increases. The variations can be gelato, frozen custard, etc. This refreshing homemade ice-cream is pure bliss. Checkout the recipe at https://www.allrecipes.com/recipe/233928/how-to-make-vanilla-ice-cream/",
        ["http://localhost:3000/public/images/vanilla.jpeg", "http://localhost:3000/public/images/ice1.jpeg", "http://localhost:3000/public/images/ice2.jpeg"],
        ["Ice-Cream","Ice-Cream-Recipe"]
    );
    let p8 = await posts.createPost(
        "Cheese garlic bread",
        u3_userid,
        "Crispy cheese garlic bread is always a hit among all!Sharing this 15-minute cheese garlic bread recipe for those folks who want a quick breakfast or brunch. On occasions, you can also make this cheesy garlic bread and serve with any soup for lunch or dinner. These can also be served as a starter snack for the get-together or parties. Use any bread and cheese of your choice. I have used brown bread and a mix of processed and cheddar cheese. You can also use mozzarella cheese. Again add the herbs or spices of your choice. Serve cheesy garlic bread with tomato ketchup or chili sauce. For detailed recipe checkout - https://www.vegrecipesofindia.com/cheese-garlic-bread-recipe/",
        ["http://localhost:3000/public/images/cheese1.jpeg","http://localhost:3000/public/images/cheese2.jpeg","http://localhost:3000/public/images/cheese3.jpg"],
        ["Breads", "Breads-Recipe"]
    );
    let p9 = await posts.createPost(
        "Brownies can be red too",
        u3_userid,
        "Super-Cooooool Red velvet brownies. I have to tell you that I really truly debated internally making these red velvet brownies with cake mix for the red velvet component of them, but I decided on a firm “no” when I seriously thought about it. I am not, however, into any kind of “cakey” business when it comes to these brownies, so I had to do a little tweaking of the recipe in order to bring red velvet cake, but leave behind the cakey-ness. I typically use whipped egg whites in my red velvet cupcakes to incorporate air. I also cream my butter and sugar to bring air to the batter as well. Link for recipe - https://freshaprilflours.com/red-velvet-brownies/",
        ["http://localhost:3000/public/images/red.jpg", "http://localhost:3000/public/images/red2.jpg", "http://localhost:3000/public/images/red3.jpg"],
        ["Brownies","Brownies-Recipe"]
    );

    //u4 create posts(Zephyr)
    let p10 = await posts.createPost(
        "Perfect Popsicles",
        u4_userid,
        "Cool Comfort Food. An ice pop is a water or milk-based frozen snack on a stick.[1] Unlike ice cream or sorbet, which are whipped while freezing to prevent ice crystal formation, an ice pop is quiescently frozen—frozen while at rest—and becomes a solid block of ice. The stick is used as a handle to hold it. Without a stick, the frozen product is known as something else, e.g., a freezie. Ice pops can be referred to as a popsicle (Canada, U.S.), freezer pop (U.S.), paleta (Mexico, Southwestern U.S.), ice lolly (United Kingdom), ice pop (U.K., Ireland, South Africa), icy pole (Australia), ice block (New Zealand / Australia), or ice drop (Philippines), ice candy (India).",
        ["http://localhost:3000/public/images/popsicle.jpeg", "http://localhost:3000/public/images/popsicle3.jpeg", "http://localhost:3000/public/images/popsicle2.jpg"],
        ["Popsicles"]
    );
    let p11 = await posts.createPost(
        "Croissants are breads",
        u4_userid,
        "Croissant is a french buttery flaky crescent shaped bread. A croissant is a buttery, flaky, viennoiserie pastry of Austrian origin, but mostly associated with France. Croissants are named for its historical crescent shape and, like other viennoiserie, are made of a layered yeast-leavened dough. The dough is layered with butter, rolled and folded several times in succession, then rolled into a thin sheet, in a technique called laminating. The process results in a layered, flaky texture, similar to a puff pastry.Crescent-shaped breads have been made since the Renaissance, and crescent-shaped cakes possibly since antiquity. Croissants have long been a staple of Austrian, Italian, and French bakeries and pâtisseries. The modern croissant was developed in the early 20th century. In the late 1970s, the development of factory-made, frozen, pre-formed but unbaked dough made them into a fast food that can be freshly baked by unskilled labor. #foodfact101",
        ["http://localhost:3000/public/images/croissant1.jpeg", "http://localhost:3000/public/images/croissant2.jpg", "http://localhost:3000/public/images/croissant3.jpeg"],
        ["Breads"]
    );
    let p12 = await posts.createPost(
        "Blueberry Ice Cream",
        u4_userid,
        "I scream, You scream, we all scream for Ice Cream! Blueberries are perennial flowering plants with blue or purple berries. They are classified in the section Cyanococcus within the genus Vaccinium. Vaccinium also includes cranberries, bilberries, huckleberries and Madeira blueberries. Commercial blueberries—both wild and cultivated —are all native to North America. In a large saucepan, combine the blueberries, sugar and water. Bring to a boil. Reduce heat; simmer, uncovered, until sugar is dissolved and berries are softened. Press mixture through a fine-mesh strainer into a bowl; discard pulp. Stir in cream. Cover and refrigerate overnight. Fill cylinder of ice cream freezer two-thirds full; freeze according to the manufacturer’s directions. (Refrigerate any remaining mixture until ready to freeze.) Transfer ice cream to freezer containers, allowing headspace for expansion. Freeze until firm, 2-4 hours. Repeat with any remaining ice cream mixture.",
        ["http://localhost:3000/public/images/blue.jpeg", "http://localhost:3000/public/images/blue2.jpeg", "http://localhost:3000/public/images/blue3.jpeg"],
        ["Ice-Cream", "Ice-Cream-Recipe"]
    );

    //add comment to p1
    let c1 = await comments.addComment(p2_postid, u3_userid, "Can't wait to try these cookies!");
    let c2 = await comments.addComment(p3_postid, u2_userid, "Cream and brownies what a combination.");

    await db.serverConfig.close();
    console.log('Done!');
}
main().catch((error) => {
    console.log(error);
});
